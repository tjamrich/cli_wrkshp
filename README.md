
---
title: "CLI wrkshp"  
author: [Tomáš Jamrich]  
git: https://gitlab.com/tjamrich/cli_wrkshp  
keywords: [CLI, BASH, Shell, Terminal]  
...

A simple cheatsheet of useful terminal commands

---

KISS && RTFM
--------------

>Try [iTerm2](https://www.iterm2.com/), it's cool.

`man <command>` - read manual pages, everything you need is in there

Organisation
--------------

1.	[cli_beginner](workshop/cli_beginner.md)
	- What is Shell
	- Moving around
	- Structure overview
	- File manipulation
	- Commands

2.	[cli_advanced](workshop/cli_advanced.md)
	- I/O Redirection
	- Expansion
	- Permissions
	- Job Control

Bludišťák
--------------

>How can you tell you are logged in as **root**?

>What's the difference between `su` and `sudo`?

>Print me a &#128007; like this the &#128026; :)

```
($\_/$)  
(='.'=)  
(")_(")  
```

Links
--------------

[BASH manual page](https://www.gnu.org/software/bash/manual/bash.html)  
[BASH Guide for Beginners](https://www.tldp.org/LDP/Bash-Beginners-Guide/html/Bash-Beginners-Guide.html)
