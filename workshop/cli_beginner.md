
---
title: "cli_beginner"  
...

---

What is Shell
--------------
The Shell is an interface which take commands from the keyboard and gives them to the OS to be performed. 
Back in the age of dinosaurs, if was the only interface available on a UNIX
systems. Now, command line interface (CLI) is just an addition to heavily used
graphical user interface (GUI).

A program called [bash](https://tiswww.case.edu/php/chet/bash/bashtop.html)
(**B**ourne **A**gain **SH**ell) is present on the majority of UNIX
systems. It acts as the shell program. There are some other shell programs as
well (`ksh`, `tcsh`, `zsh`, `fish`).

### And what about the **Terminal**?

The Terminal, is a terminal emulator. It opens a window, that lets you interact
with the shell. Of cource, there are many terminal emulators that can be used
(`gnome-terminal`, `xterm`, `rxvt`, `iterm`).

Moving around
--------------
`pwd` - print working directory  
`cd` - change directory  
`C - a` - go to line beginning
`C - e` - go to line end

>Important stuff

- file names that begin with `.` character are **hidden**. `ls` will not list them, `ls -a` will.
- file names are case sensitive &rarr; "File" and "fIlE" are different things.
- unix do not care about file extensions, however many applications do :)
- file names &rarr; **do not embed spaces** in file names (you'll thank yourself later...)
- reserved characters &rarr; `*` and `/` special characters, you can not use them in file name

Structure overview
--------------
`ls` - list files and directories, useful also `ls -l` and `ls -la`

```
-rw-------   1 cecky  logicworks       546 Apr 17  2018 file1.txt
drwxr-xr-x   6 cecky  logicworks      1024 Oct  9  2015 directory1
-rw-rw-r--   1 cecky  logicworks    125680 Feb 11 20:41 archive1.tar
-rw-------   1 cecky  logicworks      3443 Dec 16  1993 old_file.txt
----------   - -----  ----------    ------ ------------ ------------
|     |      |    |        |         |         |             |
|     |      |    |        |         |         |         File Name
|     |      |    |        |         |         |
|     |      |    |        |         |         +---  Modification Time
|     |      |    |        |         |
|     |      |    |        |         +-------------   Size (in bytes)
|     |      |    |        |
|     |      |    |        +-----------------------        Group
|     |      |    |
|     |      |    +--------------------------------        Owner
|     |      |
|     |      +-------------------------------------    Number of files
|     |
|     +--------------------------------------------   File Permissions
|
+--------------------------------------------------   Indicator (-, d, l)
```
>WTF means `t` in here?

`t` - represents a restricted deletion ("sticky") bit  

`less` - show text file contents  
`file` - determine kind of data you are about to work with  

File manipulation
--------------

`cp` - copy file/directory  
`mv` - move or rename file/directory  
`rm` - remove file/directory  
`mkdir` - create directory  
`touch` - create file

>Wildcards

`*` - match any characters  
`?` - match any single character  
`[foo]` - match any character that is a member of the character set

>POSIX character classes

`[:alnum:]` - alphanumeric chars  
`[:alpha:]` - alphabetic chars  
`[:digit:]` - numerals  
`[:upper:]` - uppercase alphabetic chars  
`[:lower:]` - lowercase alphabetic chars  

>Examples

`cecky.[[:digit]][[digit]]` - matches any file beginning with "cecky." followed by exactly 2 digits  
`*[![:upper:]]` - any filename that does **not** end with uppercase letter  

Commands
--------------

`type` - info about command type  
`which` - locate executable command  
`man` - your best friend  
`help` - a very good friend  
`<command> --help` - many executables offer help option  

Q&A - Do not try this at home :)
--------------

>What kernel modules do I have loaded?

`sudo rm -rf /` is the answer (as only something from `/proc` and `/dev` will remain ...)  

**BEWARE wildcards with `rm`, test with `ls` before**  
