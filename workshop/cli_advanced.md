
---
title: "cli_advanced"  
...

---

I/O Redirection
======

**Streams**  

> Every program you may run on the command line has 3 streams, STDIN, STDOUT and STDERR.

```
|-----------+-----------------+-----------------+------------------+------------------+---------------|
| Syntax    | StdOut (in cli) | StdErr (in cli) | StdOut (in file) | StdErr (in file) | existing file |
|-----------+-----------------+-----------------+------------------+------------------+---------------|
|-----------+-----------------+-----------------+------------------+------------------+---------------|
| >         | no              | yes             | yes              | no               | overwrite     |
| >>        | no              | yes             | yes              | no               | append        |
|-----------+-----------------+-----------------+------------------+------------------+---------------|
| 2>        | yes             | no              | no               | yes              | overwrite     |
| 2>>       | yes             | no              | no               | yes              | append        |
|-----------+-----------------+-----------------+------------------+------------------+---------------|
| &>        | no              | no              | yes              | yes              | overwrite     |
| &>>       | no              | no              | yes              | yes              | append        |
|-----------+-----------------+-----------------+------------------+------------------+---------------|
| | tee     | yes             | yes             | yes              | no               | overwrite     |
| | tee -a  | yes             | yes             | yes              | no               | append        |
|-----------+-----------------+-----------------+------------------+------------------+---------------|
| |& tee    | yes             | yes             | yes              | yes              | overwrite     |
| |& tee -a | yes             | yes             | yes              | yes              | append        |
|-----------+-----------------+-----------------+------------------+------------------+---------------|
```

Examples
------

`ls > file.txt`  
`ls >> file.txt`  
`sort < file.txt`  
`sort < file.txt > sorted_file.txt`  
`ls -l | less` - save the scrolling  
`ls -lt | head` - show 10 newest files in current dir  
`du | sort -nr` - displays list of directories and space they consume, sorted  
`find . -type f -print | wc -l` - displays total number of files in $PWD and all of its subdirectories  

Useful programs
------

`sort` - sorts stdin to stdout  
`uniq` - removes duplicates from the sorted stream of data  
`grep` - reads stdin, outputs every line that contain specified pattern  
`fmt` reads stdin, formats test before sending to stdout  
`pr` - splits text to pages, headers, footers (prepare before print)  
`head` - outputs first few lines of the input  
`tail` - tha same from the bottom end  
`tr` - translate, transliterate  
`sed` - stream editor (`tr` on steroids)  
`awk` - used to construct filters, programming languague on its own  

Expansion
======

`echo` - prints out text argument to stdout

Pathname Expansion
------

mechanism by which wildcards work

`echo [[:lower:]]*`

Tilde Expansion
------

`~` - special meaning  
> expands into the name of the home dir of the named user (or current user)

`echo ~`  
`echo ~cecky`

Arithmetic Expansion
------

`echo $((2 + 2))` - uses the form: **$((expression))**, can be nested  

Brace expansion
------

`echo Drak-{A,B,C}-Frak`  
`mkdir {2018..2019}-0{1..9} {2018..2019}-{10..12}` - sth for photographers  

Parameter expansion
------

`echo $USER`  
`echo $OLDPWD`  

Command Substitution
------

> Use the output of a command as an expansion

`ls -l $(which cp)`

Quoting 
------

> double quotes  
> single quotes  
> escape quote  

`echo $(cal)`  
`echo "$(cal)"`  

Permissions
======

`-rwxr-xr-x  1 root  wheel  610240 May  4 09:05 /bin/zsh`

> The file "/bin/zsh" is owned by user "root"  
> The superuser has the right to read, write, and execute this file  
> The file is owned by the group "wheel"  
> Members of the group "wheel" can also read and execute this file  
> Everybody else can read and execute this file  

`chmod 777 <some_file>`

**How do I get this number?**  

```
rwx = 111 in binary = 7
rw- = 110 in binary = 6
r-x = 101 in binary = 5
r-- = 100 in binary = 4
```

> r - Allows the contents of the directory to be listed if the x attribute is also set.  
> w - Allows files within the directory to be created, deleted, or renamed if the x attribute is also set.  
> x - Allows a directory to be entered.  

`chown <user> <some_file>` - requires **superuser**, works the same on files and dirs  
`chgrp <group> <some_file>` - must be **owner** of the file/dir to perform chgrp  

Job Control
======

`xload` - graph representing system load  
`<command> &` - put command in background  

**forgot to put it in bg?**  
`ctrl + z` &rarr; `bg`  

`jobs` - list of launched processes  
`kill %<job>`  

`ps` - bit more powerful than `jobs`  
`kill <process #>`  

> kill options:  
> 1 - SIGHUP  
> 2 - SIGINT  
> 15 - SIGTERM  
> 9 - SIGKILL  

Random Stuff General
======

`clear`  
`history`  
**`caffeinate`** - prevent mac from sleeping  
**`ipconfig getifaddr en0`** - get your IP  
**`curl ipecho.net/plain; echo`** - get your public IP  
**`ping -c 10 www.google.com`** - pong it dude  
**`curl -O http://pornhub.com/some_nice_video.mp4`** - download without a browser  
**`python -m SimpleHTTPServer 8000`** - simple web server anywhere you like  
`tail -f /var/log/system.log` - monitor the file output  
`mkfile 1g test.txt` - create file of any size  
`cat /path/to/file` - show contents of any file  

Random Fruit-related Stuff
======

**`say "Siri, you're fat and ugly!"`**  
`say -f /path/to/file.txt`  
`ditto` - `cp` equivalent, verbose mode, create/extract archives, copy directory hierarchies
`defaults write com.apple.finder AppleShowAllFiles -bool TRUE` - show hidden files in finder  
`killall Finder`  
`sudo fs_usage` - file system usage, Time Machine (backupd), Spotlight (mds)  
`sudo mdutil -E /Volumes/DriveName` - rebuild spotlight  
`defaults write com.apple.finder QLEnableTextSelection -bool TRUE` - enable text selection  
`sudo softwareupdate -l` - check for software updates  
`sudo softwareupdate -ia` - install all software updates  

BONUS
======

**You say your VPN is not working and you need to access your router's GUI?**  
`ssh` is the answer!  

`ssh -L 8888:<router_ip>:80 root@<router_wan_ip> -i ~/.ssh/your_mega_secret_key -p 22022`  
> Go to `localhost:8888` in your browser  
